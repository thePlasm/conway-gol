# conway-gol-scm

A game of life implementation written in scheme.

# usage

This project contains several functions that take 2D arrays of 1s and 0s as input, with 1s being live cells and 0s being dead cells.

# TODO

Add documentation regarding the different functions.
