#lang racket

(define
    list-length
    (lambda
        (list)
        (letrec
            ((len
                (lambda
                    (l i)
                    (if
                        (null? l)
                        i
                        (len (cdr l) (+ i 1))))))
            (len list 0))))

(define
    get-1d
    (lambda
        (list index)
        (if
            (or
                (null? list)
                (not (pair? list))
                (< index 0)
                (> index (- (list-length list) 1)))
            0
            (if
                (= index 0)
                (car list)
                (get-1d (cdr list) (- index 1))))))

(define
    get-2d
    (lambda
        (list x y)
        (get-1d
            (get-1d list y)
            x)))

(define
    moore-neighbours
    (lambda
        (list x y)
        (+
            (get-2d list (- x 1) y)
            (get-2d list (+ x 1) y)
            (get-2d list x (+ y 1))
            (get-2d list x (- y 1))
            (get-2d list (- x 1) (- y 1))
            (get-2d list (+ x 1) (+ y 1))
            (get-2d list (- x 1) (+ y 1))
            (get-2d list (+ x 1) (- y 1)))))

(define
    next-cell
    (lambda
        (list x y)
        (if
            (= (get-2d list x y) 0)
            (if
                (= (moore-neighbours list x y) 3)
                1
                0)
            (if
                (or
                    (= (moore-neighbours list x y) 2)
                    (= (moore-neighbours list x y) 3))
                1
                0))))

(define
    next-row
    (lambda
        (list y)
        (letrec
            ((next-row-cell
                (lambda
                    (x)
                    (if
                        (= x (list-length (get-1d list y)))
                        '()
                        (cons (next-cell list x y) (next-row-cell (+ x 1)))))))
            (next-row-cell 0))))

(define
    next-iter
    (lambda
        (list)
        (letrec
            ((next-iter-row
                (lambda
                    (y)
                    (if
                        (= y (list-length list))
                        '()
                        (cons (next-row list y) (next-iter-row (+ y 1)))))))
            (next-iter-row 0))))

(define
    conway-gol-iter
    (lambda
        (map ticks)
        (if
            (< ticks 1)
            map
            (next-iter map))))

(define
    conway-gol-list
    (lambda
        (grid ticks)
        (letrec
            ((conway-gol-list-iter
                (lambda
                    (iter-map iter-tick)
                    (if
                        (= iter-tick (+ ticks 1))
                        '()
                        (cons iter-map (conway-gol-list-iter (next-iter iter-map) (+ iter-tick 1)))))))
            (conway-gol-list-iter grid 0))))

(define
    cell->char
    (lambda
        (cell dead-char alive-char)
        (if
            (= cell 0)
            dead-char
            alive-char)))

(define
    row->string
    (lambda
        (row dead-char alive-char)
        (letrec
            ((row->list-chars
                (lambda
                    (iter-row)
                    (if
                        (or (null? iter-row) (not (pair? iter-row)))
                        '(#\newline)
                        (cons (cell->char (car iter-row) dead-char alive-char) (row->list-chars (cdr iter-row)))))))
            (list->string (row->list-chars row)))))


(define
    grid->string
    (lambda
        (map dead-char alive-char)
        (if
            (or (null? map) (not (pair? map)))
            "\n"
            (string-append (row->string (car map) dead-char alive-char) (grid->string (cdr map) dead-char alive-char)))))

(define
    list-iter->string
    (lambda
        (list dead-char alive-char)
        (if
            (or (null? list) (not (pair? list)))
            ""
            (string-append (grid->string (car list) dead-char alive-char) (list-iter->string (cdr list) dead-char alive-char)))))

(define
    conway-gol-iter-string
    (lambda
        (grid ticks dead-char alive-char)
        (grid->string (conway-gol-iter grid ticks) dead-char alive-char)))

(define
    conway-gol-list-string
    (lambda
        (grid ticks dead-char alive-char)
        (list-iter->string (conway-gol-list grid ticks) dead-char alive-char)))

(define
    next-iter-string
    (lambda
        (grid dead-char alive-char)
        (grid->string (next-iter grid) dead-char alive-char)))
